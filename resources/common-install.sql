/**
 * SQL script that will create the common schema for all database enabled nodes in the application. This script contains
 * all the common database objects. If a module will need additional object, it will be created in it's own script 
 * which should be run after this one.
 */

PRAGMA foreign_keys = OFF;

/********************************** creating tables *****************************************/

drop table if exists satellites;
create table satellites (
    id_satellite text not null primary key,
    service text not null,
    description text,
    address text,
    enabled integer not null default 1,
    unique(service),
    unique(address)
);

drop table if exists readings;
create table readings (
    id_reading text not null primary key,
    id_satellite text not null,
    timestamp text not null,
    rtype text not null,
    value real not null,
    unique(id_satellite, timestamp),
    foreign key(id_satellite) references satellites(id_satellite)
);

drop table if exists sensors;
create table sensors (
    id_sensor text not null primary key,
    id_satellite text not null,
    stype text not null,
    interval integer not null,
    _on integer not null default 1,
    foreign key(id_satellite)  references satellites(id_satellite)
);

PRAGMA foreign_keys = ON;
