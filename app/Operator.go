package app

import (
	dm "gxs3/demeter/common/model"
	gm "gxs3/godfather/model"
)

/*
 An operator is a logical client. It's a higher level abstraction of a network client. It's used to send requests to
 other nodes on the network. There will be operator implementations that will extend this type to provide custom logic
 for clients.
*/
type Operator struct {
	router        *Router // Router used to send and receive messages
	nodeService   string  // Service this node will use. Identifies this node in the network.
	brokerService string  // Service the broker will use. The broker is the intermediary that will receive messages from all nodes.

}

/*
 Creates a new operator. Receives the router which will do the actually sending and receiving of messages, and the logical
 names used to identify this node (nodeService argument) and the broker (brokerService argument) on the netowrk.
*/
func NewOperator(router *Router, nodeService, brokerService string) (*Operator, error) {
	if router == nil {
		return nil, gm.NIL_ARGUMENT
	}

	if len(nodeService) < 1 || len(brokerService) < 1 {
		return nil, gm.EMPTY_STRING
	}

	var operator = &Operator{router, nodeService, brokerService}
	return operator, nil
}

/*
 Sends the request out through the router. If an encoder is provided (non-nil argument), it is used to encode the body
 field of the message
*/
func (o *Operator) Send(request *dm.Request, encoder dm.BodyEncoder) error {
	if request == nil {
		return gm.NIL_ARGUMENT
	}

	request.SetSource(o.nodeService)
	request.SetDestination(o.brokerService)
	var message, err = request.Encode(encoder)

	if err != nil {
		return err
	}

	o.router.out <- message
	return nil
}
