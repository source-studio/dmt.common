package app

import (
	dm "gxs3/demeter/common/model"
	gm "gxs3/godfather/model"
)

const (
	OK                    = 200 // Request was handled successfully
	CREATED               = 201 // Resource was created successfully
	BAD_REQUEST           = 400 // Request was not sent correctly
	FORBIDDEN             = 403 // Request cannot be executed. Not due to a lack of privileges.
	NOT_FOUND             = 404 // Requested resources could not be found
	REQUEST_TIMEOUT       = 408 // Timeout has elapsed on request
	CONFLICT              = 409 // A conflict in data occured while processing the transaction
	PRECONDITION_FAILED   = 412 // A precondition required for the request to be completed was insatisfactory
	INTERNAL_SERVER_ERROR = 500 // Internal server error. Usually a localized crash or unhandled error.
	NOT_IMPLEMENTED       = 501 // The server has not implemented the handler for the request
	SERVICE_UNAVAILABLE   = 503 // The server cannot respond at this moment

	SATELLITE_GET       = "dmt.satellite.get"       // Satellite get request type
	SATELLITE_UPDATE    = "dmt.satellite.update"    // Satellite update request type
	SATELLITE_CALIBRATE = "dmt.satellite.calibrate" // Satellite calibrate request type
	READING_ADD         = "dmt.reading.add"         // Reading add request type
)

/*
 Handler function type definition. Functions that are compatible with this type will recieve a slice of bytes and may
 return an error. Implementations are expected to handle a request coming in from another node on the network. If a
 reply is necessary, it should be sent by the implementation, preferably through the router.
*/
type Handle func(message []byte) error

/*
 Handler base type. Most handlers sould extend this type. It includes a reference to the message router, which will be
 used to send and receive messages.
*/
type Handler struct {
	router *Router
}

/*
 Creates a new handler. Receives the router that will be use to route incoming and outgoing messages
*/
func NewHandler(router *Router) (*Handler, error) {
	if router == nil {
		return nil, gm.NIL_ARGUMENT
	}

	var handler = &Handler{router}
	return handler, nil
}

/*
 Registers a handler function with the router. The implementation must provide the message type (mtype argument), which
 can either be request or response. They must also pass they request type (rtype argument). This will depend on the handler
 implementation. Each handler will expose different request types they will offer to handle. Lastly, the function that
 will be invoked when a message that fits the criteria arrives.
*/
func (h *Handler) Register(mtype dm.MessageType, rtype dm.RequestType, handler Handle) error {
	return h.router.Register(mtype, rtype, handler)
}

/*
 Sends a response through the router. If an encoder is provided (non-nil value) it is used to encode the body field
 of the message. The respones object should have details specifiying the recepient of the message
*/
func (h *Handler) Send(response *dm.Response, encoder dm.BodyEncoder) error {
	if response == nil {
		return gm.NIL_ARGUMENT
	}

	var message, err = response.Encode(encoder)

	if err != nil {
		return err
	}

	h.router.out <- message
	return nil
}
