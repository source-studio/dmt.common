/*
 Application logic for the common library. Holds most of the logic that is shared among modules.
*/
package app

import (
	"bufio"
	"encoding/json"
	"io"
	"os"
	"strings"
)

/*
 Base implementation of the config file parser/writer. Other modules will create extensions of this implementation in
 order to read/write their own specific values and structures. Config file is a json format.
*/
type BaseConfig struct{}

/*
 Reads the file for the given path and attempts to parse it as a json object. Getting values out of this json object
 is left to the config implemtations in other modules. Any lines beginning with '//' or '#' are considered comments
 and ignored.
*/
func (bc *BaseConfig) Read(path string) (map[string]interface{}, error) {
	_, err := os.Stat(path)

	if err != nil && os.IsNotExist(err) {
		panic("Config file was not found at expected location: " + path)
	} else if err != nil {
		panic("Error attempting to open config file: " + err.Error())
	}

	file, err := os.Open(path)

	if err != nil {
		return nil, err
	}

	defer file.Close()
	var reader = bufio.NewReader(file)
	var buffer = make([]byte, 0)

	for {
		line, err := reader.ReadBytes('\n')

		if err != nil {
			if err == io.EOF {
				break
			}

			return nil, err
		}

		var lineStr = strings.TrimSpace(string(line))

		if strings.HasPrefix(lineStr, "//") || strings.HasPrefix(lineStr, "#") {
			continue
		}

		buffer = append(buffer, line...)
	}

	var raw interface{}
	err = json.Unmarshal(buffer, &raw)

	if err != nil {
		return nil, err
	}

	return raw.(map[string]interface{}), nil
}

/*
 Writes the given json data to the specified path. Indents the json to provide a pretty printed representation.
*/
func (bc *BaseConfig) Write(path string, job map[string]interface{}) error {
	file, err := os.Open(path)

	if err != nil {
		return err
	}

	defer file.Close()
	buffer, err := json.MarshalIndent(job, "", "    ")

	if err != nil {
		return err
	}

	var writer = bufio.NewWriter(file)
	_, err = writer.Write(buffer)

	if err != nil {
		return err
	}

	return nil
}
