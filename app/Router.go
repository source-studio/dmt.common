package app

import (
	"gxs3/demeter/common/model"
	"gxs3/demeter/common/net"
	gm "gxs3/godfather/model"
	"log"
)

/*
 The router will do the actual sending/receiving of messages from/to different nodes on the network. It will be used
 by handlers (logical services) and operators (logical clients) to send and receive messages respectively.
*/
type Router struct {
	out      chan [][]byte       // Channel used to pass message that will be sent
	client   net.Client          // Network client that will be doing the physical emission
	worker   net.Worker          // Netowrk worker that will be physically receiving messages
	handlers map[string][]Handle // Registered handlers. These will provide processing for incoming requests
	closing  bool                // Indicates if the router is being closed, and should stop listening for requests
}

/*
 Creates a new router. Receives the address from the broker (middlepoint for all nodes on the network) and the service
 name for this node (service)
*/
func NewRouter(brokerAddress, service string) *Router {
	var out = make(chan [][]byte)
	var handlers = make(map[string][]Handle)
	var client = net.NewClient(brokerAddress, false)
	var worker = net.NewWorker(brokerAddress, service, false)

	var router = &Router{out, client, worker, handlers, false}
	return router
}

/*
 Initializes the router. Kicks off the send and receive routines.
*/
func (r *Router) Start() {
	go r.send()
	r.receive()
}

/*
 Registers a handler with the router. When a message is received that matches the message and request types, it will
 be passed to the handler for processing. Multiple handlers can register for the same message/request types. The
 message will be passed to each one in the order they registered.
*/
func (r *Router) Register(mtype model.MessageType, rtype model.RequestType, handler Handle) error {
	if len(mtype) < 1 || len(rtype) < 1 {
		return gm.EMPTY_STRING
	}

	if handler == nil {
		return gm.NIL_ARGUMENT
	}

	var key = string(mtype) + "|" + string(rtype)
	var existing, ok = r.handlers[key]

	if ok {
		r.handlers[key] = append(existing, handler)
	} else {
		r.handlers[key] = make([]Handle, 1)
		r.handlers[key][0] = handler
	}

	return nil
}

/*
 Returns a reference to the outgoing channel. Used to pass messages to the routine which sends messages to the broker.
*/
func (r *Router) Out() chan [][]byte {
	return r.out
}

/*
 Closes the router. Stops the sending and receiving routines, and closes the network client and worker.
*/
func (r *Router) Close() {
	r.closing = true
	r.worker.Close()
	r.client.Close()
}

/*
 Implements the routine that is used to receive messages from other nodes. It will block waiting for a new message.
 When a new message is received, it will look for handlers which have been registered to process this kind of request.
 All that are found are passed the request and given a chance to process it.
*/
func (r *Router) receive() {
	for !r.closing {
		var container = r.worker.Recv(nil)
		var message = container[0]
		var mtype, rtype, err = model.Identify(message)

		if err != nil {
			log.Println("Error: Unexepcted message type: ", mtype)
			continue
		}

		var handlers, ok = r.handlers[string(mtype)+"|"+string(rtype)]

		if !ok {
			continue
		}

		for _, handler := range handlers {
			handlerErr := handler(message)

			if handlerErr != nil {
				log.Println("Error processing request [", mtype, "|", rtype, "]:", handlerErr.Error())
				request, err := model.DecodeRequest(message, nil)

				if err != nil {
					continue
				}

				var response = request.MakeResponse(INTERNAL_SERVER_ERROR, handlerErr.Error())
				respmesg, err := response.Encode(nil)

				if err != nil {
					continue
				}

				r.out <- respmesg
			}
		}
	}
}

/*
 Implements the routine for sending messages. Messages are expected to be sent to the out channel which can be accessed
 through the Out() method. However, Operators will like abstract this procedure, so it's best to send requests through
 them. Although if low level message passing is required, it can be done through the out channel.
*/
func (r *Router) send() {
	for !r.closing {
		var message = <-r.out

		if len(message) < 2 {
			log.Println("Invalid message: ", message)
		}

		r.client.Send(message[0], [][]byte{message[1]})
	}
}
