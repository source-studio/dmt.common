package data

import (
	"database/sql"
	"gxs3/demeter/common/model"
	"gxs3/godfather/data"
)

/*
 Satellite data access object
*/
type SatelliteDao struct {
	*data.BaseDbDao
}

/*
 Creates a new satellite data access object. Requires the sqltemplate and query dictionary
*/
func NewSatelliteDao(template *data.SqlTemplate, dictionary map[string]string) (*SatelliteDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var dao = &SatelliteDao{base}
	return dao, nil
}

/*
 Maps a satellite object from a sql row
*/
func mapSatellite(rows *sql.Rows) (interface{}, error) {
	var enabled uint
	var id, service, description, address string

	var err = rows.Scan(&id, &service, &description, &address, &enabled)

	if err != nil {
		return nil, err
	}

	return model.NewSatellite(id, service, description, address, enabled > 0)
}

/*
 Retrieves a satellite as identified by a given ID
*/
func (dao *SatelliteDao) Get(id uint) (*model.Satellite, error) {
	var query = dao.GetQuery("dmt.common.satellite-get")
	var result, err = dao.Template().SelectFirst(query, mapReading, id)

	if result == nil || err != nil {
		return nil, err
	}

	var satellite = result.(*model.Satellite)
	return satellite, nil
}

/*
 Retrieves all available satellites
*/
func (dao *SatelliteDao) GetAll() ([]*model.Satellite, error) {
	var query = dao.GetQuery("dmt.common.satellite-get-all")
	var result, err = dao.Template().Select(query, mapSatellite)

	return dao.cast(result), err
}

/*
 Retrieves satellites by enabled state
*/
func (dao *SatelliteDao) GetByEnabled(enabled bool) ([]*model.Satellite, error) {
	var query = dao.GetQuery("dmt.common.satellite-get-by-enabled")
	var result, err = dao.Template().Select(query, mapSatellite, data.ToNumber(enabled))

	return dao.cast(result), err
}

/*
 Adds a new satellite to the collection
*/
func (dao *SatelliteDao) Add(satellite *model.Satellite) (uint, error) {
	var query = dao.GetQuery("dmt.common.satellite-insert")
	var result, err = dao.Template().Insert(query,
		satellite.Id(),
		satellite.Service(),
		satellite.Description(),
		satellite.Address(),
		data.ToNumber(satellite.Enabled()),
	)

	err = data.CheckConstraintError(err)
	return uint(result), err
}

/*
 Updates the details of an existing satellite. Uses the ID as a reference point and updates all other fields.
*/
func (dao *SatelliteDao) Update(satellite *model.Satellite) (uint, error) {
	var query = dao.GetQuery("dmt.common.satelite-update")
	var result, err = dao.Template().Update(query,
		satellite.Service(),
		satellite.Description(),
		satellite.Address(),
		data.ToNumber(satellite.Enabled()),
		satellite.Id(),
	)

	return uint(result), err
}

/*
 Removes a satellite identified by the given ID
*/
func (dao *SatelliteDao) Remove(id uint) (uint, error) {
	var query = dao.GetQuery("dmt.common-satellite-remove")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/*
 Utility that casts a slice of objects into Satellite type
*/
func (dao *SatelliteDao) cast(result []interface{}) []*model.Satellite {
	if result == nil {
		return nil
	}

	var satellites = make([]*model.Satellite, len(result))

	for cont, s := range result {
		satellites[cont] = s.(*model.Satellite)
	}

	return satellites
}
