/*
 Data package provides an abstraction that makes database transaction easier for the upper layers
*/
package data

import (
	"database/sql"
	"gxs3/demeter/common/model"
	"gxs3/godfather/data"
	"time"
)

/*
 Reading data access object
*/
type ReadingDao struct {
	*data.BaseDbDao
}

/*
 Creates a new reading data access object. Requires the sqltemplate to make database transactions, and the query dictionary
*/
func NewReadingDao(template *data.SqlTemplate, dictionary map[string]string) (*ReadingDao, error) {
	var base, err = data.NewBaseDbDao(template, dictionary)

	if err != nil {
		return nil, err
	}

	var dao = &ReadingDao{base}
	return dao, nil
}

/*
 Utility method to map a reading from a sql row.
*/
func mapReading(rows *sql.Rows) (interface{}, error) {
	var id, idSatellite, timestampStr, rtype string
	var value float32

	var err = rows.Scan(&id, &idSatellite, &timestampStr, &rtype, &value)

	if err != nil {
		return nil, err
	}

	timestamp, err := time.Parse(data.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	return model.NewReading(id, idSatellite, timestamp, model.ReadingType(rtype), value)
}

/*
 Retrieves the reading identified by the given ID
*/
func (dao *ReadingDao) Get(id string) (*model.Reading, error) {
	var query = dao.GetQuery("dmt.common.reading-get")
	var result, err = dao.Template().SelectFirst(query, mapReading, id)

	if result == nil || err != nil {
		return nil, err
	}

	var reading = result.(*model.Reading)
	return reading, nil
}

/*
 Retrieves all available readings
*/
func (dao *ReadingDao) GetAll() ([]*model.Reading, error) {
	var query = dao.GetQuery("dmt.common.reading-get-all")
	var result, err = dao.Template().Select(query, mapReading)

	return dao.cast(result), err
}

/*
 Only retrieves the readings that match the specified type and time range
*/
func (dao *ReadingDao) GetByType(rtype string, from, to time.Time) ([]*model.Reading, error) {
	var query = dao.GetQuery("dmt.common.reading-get-by-type")
	var result, err = dao.Template().Select(query, mapReading,
		rtype,
		from.Format(data.DATE_TIME_FORMAT),
		to.Format(data.DATE_TIME_FORMAT),
	)

	return dao.cast(result), err
}

/*
 Only retrieves the readings that match the specified reading type, satellite and time range
*/
func (dao *ReadingDao) GetByTypeSatellite(rtype string, idSatellite uint, from, to time.Time) ([]*model.Reading, error) {
	var query = dao.GetQuery("dmt.common.reading-get-by-type-satellite")
	var result, err = dao.Template().Select(query, mapReading,
		rtype,
		idSatellite,
		from.Format(data.DATE_TIME_FORMAT),
		to.Format(data.DATE_TIME_FORMAT),
	)

	return dao.cast(result), err
}

/*
 Adds a reading to the collection
*/
func (dao *ReadingDao) Add(reading *model.Reading) (uint, error) {
	var query = dao.GetQuery("dmt.common.reading-insert")
	var result, err = dao.Template().Insert(query,
		reading.Id(),
		reading.IdSatellite(),
		reading.Timestamp().Format(data.DATE_TIME_FORMAT),
		reading.Type(),
		reading.Value(),
	)

	err = data.CheckConstraintError(err)
	return uint(result), err
}

/*
 Updates an existing reading. Uses the ID as a reference point. All other fields are updated.
*/
func (dao *ReadingDao) Update(reading *model.Reading) (uint, error) {
	var query = dao.GetQuery("dmt.common.reading-update")
	var result, err = dao.Template().Update(query, reading.Value(), reading.Id())

	return uint(result), err
}

/*
 Removes an existing reading, identified by a given ID
*/
func (dao *ReadingDao) Remove(id string) (uint, error) {
	var query = dao.GetQuery("dmt.common.reading-remove")
	var result, err = dao.Template().Update(query, id)

	return uint(result), err
}

/*
 Removes all available readings
*/
func (dao *ReadingDao) RemoveAll() (uint, error) {
	var query = dao.GetQuery("dict.common-reading-remove-all")
	var result, err = dao.Template().Update(query)

	return uint(result), err
}

/*
 Utility used to cast all elements in a slice to Reading type
*/
func (dao *ReadingDao) cast(result []interface{}) []*model.Reading {
	if result == nil {
		return nil
	}

	var readings = make([]*model.Reading, len(result))

	for cont, r := range result {
		readings[cont] = r.(*model.Reading)
	}

	return readings
}
