package model

import (
	"encoding/json"
	"gxs3/godfather/model"
)

const (
	RESPONSE MessageType = "dmt.response" // Response message type constant
)

/*
 Response message definition. This is the second message in the request-response dynamic. It is used to send a reply
 to the node that originally sent a request. Responses are usually created with the MakeResponse method in the Request
 object. This takes care of building a response that matches the criteria of the request, and will be recognized as
 such by the other node. However, responses can be created independently and used if correctly constructed.
*/
type Response struct {
	*Message
	requestId string // ID of the request for which this reply is generated
	status    uint   // Status code indicating result of processing. Based on HTTP status codes.
}

/*
 Cretes a new response. If an ID is not provided one is generated automatically. The request type must match the original
 request. The requestId must match the ID of the request. The source is the request's destination and vice, versa.
 A subset of HTTP status codes are used to provide status. The body can be nil, but if provided it will be included with
 the response. Encoder / Decoder implementations will be required to serialize and serialize the body.
*/
func NewResponse(id string, rtype RequestType, requestId, source, destination string, status uint, body interface{}) (*Response, error) {
	if len(requestId) < 1 {
		return nil, model.EMPTY_STRING
	}

	var message, err = NewMessage(id, RESPONSE, rtype, source, destination, body)

	if err != nil {
		return nil, err
	}

	var response = &Response{message, requestId, status}
	return response, nil
}

/*
Decodes a response. If a message body is not expected, the decoder can be passed as a nil value.
*/
func DecodeResponse(message []byte, decoder BodyDecoder) (*Response, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 7)
	var id, rtype, requestId, source, destination string
	var status float64
	var bodyraw interface{}

	id, ok[0] = job["Id"].(string)
	rtype, ok[1] = job["Rtype"].(string)
	requestId, ok[2] = job["RequestId"].(string)
	source, ok[3] = job["Source"].(string)
	destination, ok[4] = job["Destination"].(string)
	status, ok[5] = job["Status"].(float64)
	bodyraw, ok[6] = job["Body"]

	for _, val := range ok {
		if !val {
			return nil, model.BAD_VALUE
		}
	}

	body, err := decoder(bodyraw)

	if err != nil {
		return nil, err
	}

	response, err := NewResponse(
		id,
		RequestType(rtype),
		requestId,
		source,
		destination,
		uint(status),
		body,
	)

	return response, err
}

/*
 Encodes a response. The encoder is only required if there is a body attached to the message.
*/
func (r *Response) Encode(encoder BodyEncoder) ([][]byte, error) {
	var bodyjob interface{} = nil

	if encoder != nil {
		bodyjob = encoder(r.body)
	} else {
		bodyjob = r.body
	}

	var model = struct {
		Id          string
		Mtype       string
		Rtype       string
		RequestId   string
		Source      string
		Destination string
		Status      uint
		Body        interface{}
	}{
		r.id,
		string(r.mtype),
		string(r.rtype),
		r.requestId,
		r.source,
		r.destination,
		r.status,
		bodyjob,
	}

	var content, err = json.Marshal(model)

	if err != nil {
		return nil, err
	}

	return [][]byte{
		[]byte(r.destination),
		content,
	}, nil
}

func (r *Response) RequestId() string {
	return r.requestId
}

func (r *Response) SetRequestId(requestId string) error {
	if len(requestId) < 1 {
		return model.EMPTY_STRING
	}

	r.requestId = requestId
	return nil
}

func (r *Response) Status() uint {
	return r.status
}

func (r *Response) SetStatus(status uint) {
	r.status = status
}
