package model

import (
	"gxs3/godfather/model"
	"time"
)

//TODO: Define constant values for all reading types. One sensor may give off multiple types of different readings, so
// it's different from SensorType, although there will be some overlap.

/*
 Readings lie at the center of the application, and is probably the most important model. Satellites will have one or
 more sensors attached to them. Each sensor will collection one or more types of readings which will be sent to the
 broker. These readins will hold climate data, and are the basis for all analysis done with the application.
*/
type Reading struct {
	id          string // Unique ID for the reading. Will be a UUID
	idSatellite string // ID of the satellite that is connected
	timestamp   time.Time
	rtype       ReadingType
	value       float32
}

type ReadingType string

/*
 Creates a new reading
*/
func NewReading(id, idSatellite string, timestamp time.Time, rtype ReadingType, value float32) (*Reading, error) {
	if len(rtype) < 1 {
		return nil, model.EMPTY_STRING
	}

	if len(id) < 1 {
		uuid, _ := NewV4()
		id = uuid.String()
	}

	var r = &Reading{id, idSatellite, timestamp, rtype, value}
	return r, nil
}

/*
 Decodes a reading from a raw json object
*/
func DecodeReading(jobraw interface{}) (interface{}, error) {
	if jobraw == nil {
		return nil, nil
	}

	var job, jok = jobraw.(map[string]interface{})

	if !jok {
		return nil, model.BAD_VALUE
	}

	var ok = make([]bool, 5)
	var id, rtype, idSatellite, timestampStr string
	var value float64

	id, ok[0] = job["Id"].(string)
	idSatellite, ok[1] = job["IdSatellite"].(string)
	timestampStr, ok[2] = job["Timestamp"].(string)
	rtype, ok[3] = job["Rtype"].(string)
	value, ok[4] = job["Value"].(float64)

	for _, val := range ok {
		if !val {
			return nil, model.BAD_VALUE
		}
	}

	timestamp, err := time.Parse(model.DATE_TIME_FORMAT, timestampStr)

	if err != nil {
		return nil, err
	}

	reading, err := NewReading(
		id,
		idSatellite,
		timestamp,
		ReadingType(rtype),
		float32(value),
	)

	return reading, err
}

/*
 Encodes a reading into a raw json object
*/
func EncodeReading(r interface{}) interface{} {
	if r == nil {
		return nil
	}

	var reading = r.(*Reading)
	var job = make(map[string]interface{})

	job["Id"] = reading.id
	job["IdSatellite"] = reading.idSatellite
	job["Timestamp"] = reading.timestamp.Format(model.DATE_TIME_FORMAT)
	job["RType"] = reading.rtype
	job["Value"] = reading.value

	return job
}

func (r *Reading) Id() string {
	return r.id
}

func (r *Reading) SetId(id string) {
	if len(id) < 1 {
		uuid, _ := NewV4()
		id = uuid.String()
	}

	r.id = id
}

func (r *Reading) IdSatellite() string {
	return r.idSatellite
}

func (r *Reading) SetIdSatellite(idSatellite string) error {
	if len(idSatellite) < 1 {
		return model.EMPTY_STRING
	}

	r.idSatellite = idSatellite
	return nil
}

func (r *Reading) Timestamp() time.Time {
	return r.timestamp
}

func (r *Reading) SetTimestamp(timestamp time.Time) {
	r.timestamp = timestamp
}

func (r *Reading) Type() ReadingType {
	return r.rtype
}

func (r *Reading) SetType(rtype ReadingType) error {
	if len(rtype) < 1 {
		return model.EMPTY_STRING
	}

	r.rtype = rtype
	return nil
}

func (r *Reading) Value() float32 {
	return r.value
}

func (r *Reading) SetValue(value float32) {
	r.value = value
}
