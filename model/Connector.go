/*
 This package defines most of the models that will be used in all of the modules. They are used to pass data around
 inside and outside of modules.
*/
package model

import (
	"gxs3/godfather/model"
)

/*
 Provides the details required to make a serial connection to a sensor
*/
type Connector struct {
	port string // Port or device
	baud int    // Baud rate
}

/*
 Creates a new connector
*/
func NewConnector(port string, baud int) (*Connector, error) {
	if len(port) < 1 {
		return nil, model.EMPTY_STRING
	}

	if baud < 1 {
		return nil, model.OUT_OF_RANGE
	}

	var c = &Connector{port, baud}
	return c, nil
}

func (c *Connector) Port() string {
	return c.port
}

func (c *Connector) SetPort(port string) error {
	if len(port) < 1 {
		return model.EMPTY_STRING
	}

	c.port = port
	return nil
}

func (c *Connector) Baud() int {
	return c.baud
}

func (c *Connector) SetBaud(baud int) error {
	if baud < 1 {
		return model.OUT_OF_RANGE
	}

	c.baud = baud
	return nil
}
