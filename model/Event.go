package model

import (
	"gxs3/godfather/model"
)

/*
 Defines the interface that is used to identify an event in the system
*/
type Event interface {
	String() string      // String representation of the event
	Type() EventType     // Event type
	Source() interface{} // Object that raised the event
	Stopped() bool
}

/*
 Type used to identify different event types
*/
type EventType string

/*
 Base implementation of an event. Other types will use this as a base to define more specific events.
*/
type BaseEvent struct {
	etype   EventType
	source  interface{}
	stopped bool
}

/*
 Creates a new base event. Required the event type
*/
func NewBaseEvent(etype EventType) (*BaseEvent, error) {
	if len(etype) < 1 {
		return nil, model.EMPTY_STRING
	}

	var e = &BaseEvent{etype: etype}
	return e, nil
}

/*
 Provides a string representation for the event. Informs the type
*/
func (e *BaseEvent) String() string {
	return string(e.etype) + " event"
}

func (e *BaseEvent) Type() EventType {
	return e.etype
}

func (e *BaseEvent) Source() interface{} {
	return e.source
}

func (e *BaseEvent) SetSource(source interface{}) {
	e.source = source
}

func (e *BaseEvent) Stopped() bool {
	return e.stopped
}

func (e *BaseEvent) StopPropagation() {
	e.stopped = true
}
