package model

import (
	"fmt"
	"gxs3/godfather/model"
)

const (
	READING_EVENT EventType = "reading" // Reading event type
)

/*
 Event raised when a reading is taken
*/
type ReadingEvent struct {
	*BaseEvent
	rtype ReadingType // Type of reading
	value float32     // Value of reading
}

/*
 Creates a new reading event. Requires the type and value
*/
func NewReadingEvent(rtype ReadingType, value float32) (*ReadingEvent, error) {
	if len(rtype) < 1 {
		return nil, model.EMPTY_STRING
	}

	var event, err = NewBaseEvent(READING_EVENT)

	if err != nil {
		return nil, err
	}

	return &ReadingEvent{event, rtype, value}, nil
}

func (re *ReadingEvent) String() string {
	return re.BaseEvent.String() + fmt.Sprintf(" - Type: %s, Value: %f", re.rtype, re.value)
}

func (re *ReadingEvent) RType() ReadingType {
	return re.rtype
}

func (re *ReadingEvent) SetRType(rtype ReadingType) error {
	if len(rtype) < 1 {
		return model.EMPTY_STRING
	}

	re.rtype = rtype
	return nil
}

func (re *ReadingEvent) Value() float32 {
	return re.value
}

func (re *ReadingEvent) SetValue(value float32) {
	re.value = value
}
