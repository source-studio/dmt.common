package model

import (
	"encoding/json"
	"gxs3/godfather/model"
)

const (
	REQUEST MessageType = "dmt.request" // Request message tyep constant
)

/*
 Request message type definition. Used for all messages that initiate a request-response message dynamic. All requests
 will have a REQUEST message type. It's request type will depend on the operator implementation.
*/
type Request struct {
	*Message
}

/*
 Creates a new request. Expects a request type. If no body is required then nil can be passed.
*/
func NewRequest(rtype RequestType, body interface{}) (*Request, error) {
	var message, err = NewMessage("", REQUEST, rtype, "", "", body)

	if err != nil {
		return nil, err
	}

	var Request = &Request{message}
	return Request, nil
}

/*
 Decodes an incoming request. Because the message body can be of any type, the body decoder will be needed to provide
 the specific decoding logic. If no body is expected within the mssage, then nil can be passed in the decoder
 argument.
*/
func DecodeRequest(message []byte, decoder BodyDecoder) (*Request, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return nil, err
	}

	var job = raw.(map[string]interface{})
	var ok = make([]bool, 5)
	var id, source, destination, rtype string
	var bodyraw, body interface{}

	id, ok[0] = job["Id"].(string)
	rtype, ok[1] = job["Rtype"].(string)
	source, ok[2] = job["Source"].(string)
	destination, ok[3] = job["Destination"].(string)
	bodyraw, ok[4] = job["Body"]

	for _, val := range ok {
		if !val {
			return nil, model.BAD_VALUE
		}
	}

	if decoder != nil {
		body, err = decoder(bodyraw)

		if err != nil {
			return nil, err
		}
	}

	request, err := NewRequest(
		RequestType(rtype),
		body,
	)
	request.SetId(id)
	request.SetSource(source)
	request.SetDestination(destination)

	return request, err
}

/*
 Encodes a request. Because the message body can be of any type, a body encoder will be needed to provide the specific
 encoding logic. If no body is expected within the message, then nil can be passed in the encoder argument.
*/
func (r *Request) Encode(encoder BodyEncoder) ([][]byte, error) {
	var bodyjob interface{} = nil

	if encoder != nil {
		bodyjob = encoder(r.body)
	}

	var model = struct {
		Id          string
		Mtype       string
		Rtype       string
		Source      string
		Destination string
		Body        interface{}
	}{
		r.id,
		string(r.mtype),
		string(r.rtype),
		r.source,
		r.destination,
		bodyjob,
	}

	var content, err = json.Marshal(model)

	if err != nil {
		return nil, err
	}

	return [][]byte{
		[]byte(r.destination),
		content,
	}, nil
}

/*
 Creates a response based on this request. The same request type is used. The request's ID is used as requestID argument
 in the response. The response's destination is the requet's source, and vice versa. The invoker must pass a status code.
 A subset of the HTTP status codes are used to keep things simple. If a body is required in the response, it is passed
 as well, otherwise nil is acceptable.
*/
func (r *Request) MakeResponse(status uint, body interface{}) *Response {
	var response, _ = NewResponse("", r.rtype, r.id, r.destination, r.source, status, body)
	return response
}
