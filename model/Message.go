package model

import (
	"encoding/json"
	"gxs3/godfather/model"
)

/*
 Defines a basic message that will be passed between nodes on the network. This is extended by Request and Response,
 which add their own specific logistics.
*/
type Message struct {
	id          string      // unique identifier for the message
	mtype       MessageType // Message type. Usually is request or response
	rtype       RequestType // Request type. Used to identify the handler that will process the message.
	source      string      // Node which originated the message
	destination string      // Node which will receive the message
	body        interface{} // Message body. Can be nil or any value. Relies on custom encoder/decoders.
}

/*
 Type used to identify message types. These tell the router how to process a mesage.
*/
type MessageType string

/*
 Type used to identify request types. These help the router identify which handler(s) are eligible to process the request.
*/
type RequestType string

/*
 Function type definition for message body decoders. Messages have a body that can be of any type. Custom decoders are
 created to handle specific message types. The argument and return types cannot be defined staticly, since we don't know
 before hand what will be deocded. The Handlers / Operators will need to know the type they are dealing with.
*/
type BodyDecoder func(interface{}) (interface{}, error)

/*
 Function type definition for message boy encoders. Messages have a body tha tcan be of anytype. Custom encoders are
 created to handle specific message types. The argument and return types cannot be defined staticly, since we don't know
 before hand what will be encoded. The Handlers / Operators will need to know the type they are dealing with.
*/
type BodyEncoder func(interface{}) interface{}

/*
 Creates a new message. If an ID is not provided, one will be generated automatically. This is recommended for new requests.
 If no body is required (for example: status responses), a nil value can be provided.
*/
func NewMessage(id string, mtype MessageType, rtype RequestType, source, destination string, body interface{}) (*Message, error) {
	if len(mtype) < 1 || len(rtype) < 1 {
		return nil, model.EMPTY_STRING
	}

	if len(id) < 1 {
		var uuid, _ = NewV4()
		id = uuid.String()
	}

	var message = &Message{id, mtype, rtype, source, destination, body}
	return message, nil
}

/*
 Identifies message and request type for the given message. Used during routing procedures.
*/
func Identify(message []byte) (MessageType, RequestType, error) {
	var raw interface{}
	var err = json.Unmarshal(message, &raw)

	if err != nil {
		return "", "", err
	}

	var job = raw.(map[string]interface{})
	var mtype, ok = job["mtype"].(string)

	if !ok {
		return "", "", model.BAD_VALUE
	}

	rtype, ok := job["rtype"].(string)

	if !ok {
		return "", "", model.BAD_VALUE
	}

	return MessageType(mtype), RequestType(rtype), nil
}

/*
 Generic decoder used to retrieve integer values from the essage body
*/
func IntDecoder(value interface{}) (interface{}, error) {
	var uv, ok = value.(uint)

	if !ok {
		return nil, model.BAD_VALUE
	}

	return uv, nil
}

func (m *Message) Id() string {
	return m.id
}

func (m *Message) SetId(id string) {
	if len(id) < 1 {
		uuid, _ := NewV4()
		id = uuid.String()
	}

	m.id = id
}

func (m *Message) Mtype() MessageType {
	return m.mtype
}

func (m *Message) SetMtype(mtype MessageType) error {
	if len(mtype) < 1 {
		return model.EMPTY_STRING
	}

	m.mtype = mtype
	return nil
}

func (m *Message) Rtype() RequestType {
	return m.rtype
}

func (m *Message) SetRtype(rtype RequestType) error {
	if len(rtype) < 1 {
		return model.EMPTY_STRING
	}

	m.rtype = rtype
	return nil
}

func (m *Message) Source() string {
	return m.source
}

func (m *Message) SetSource(source string) error {
	if len(source) < 1 {
		return model.EMPTY_STRING
	}

	m.source = source
	return nil
}

func (m *Message) Destination() string {
	return m.destination
}

func (m *Message) SetDestination(destination string) error {
	if len(destination) < 1 {
		return model.EMPTY_STRING
	}

	m.destination = destination
	return nil
}

func (m *Message) Body() interface{} {
	return m.body
}

func (m *Message) SetBody(body interface{}) {
	m.body = body
}
