package model

import (
	"gxs3/godfather/model"
)

/*
 Satellites are the workhorses of the system. They will be spread out in the greenhouse collecting sensor readings. There
 will be different types of satellites. Each type will have different combinations of different sensors to provide
 readings in different contexts. Terminals will need to keep track of available satellites in order to know who to
 expect readings from, and who to send commands to. This is where this model comes into play.
*/
type Satellite struct {
	id          string
	service     string
	description string
	address     string
	enabled     bool
}

/*
 Creates a new satellite
*/
func NewSatellite(id, service, description, address string, enabled bool) (*Satellite, error) {
	if len(service) < 1 || len(address) < 1 {
		return nil, model.EMPTY_STRING
	}

	if len(id) < 1 {
		uuid, _ := NewV4()
		id = uuid.String()
	}

	var s = &Satellite{id, service, description, address, enabled}
	return s, nil
}

/*
 Decodes a satellite
*/
func DecodeSatellite(jobraw interface{}) (interface{}, error) {
	if jobraw == nil {
		return nil, nil
	}

	var job, jok = jobraw.(map[string]interface{})

	if !jok {
		return nil, model.BAD_VALUE
	}

	var ok = make([]bool, 5)
	var id, service, description, address string
	var enabled bool

	id, ok[0] = job["Id"].(string)
	service, ok[1] = job["Service"].(string)
	description, ok[2] = job["Description"].(string)
	address, ok[3] = job["Address"].(string)
	enabled, ok[4] = job["Enabled"].(bool)

	for _, val := range ok {
		if !val {
			return nil, model.BAD_VALUE
		}
	}

	satellite, err := NewSatellite(
		id,
		service,
		description,
		address,
		enabled,
	)

	return satellite, err
}

/*
Encodes a satellite
*/
func EncodeSatellite(s interface{}) interface{} {
	if s == nil {
		return nil
	}

	var satellite = s.(*Satellite)
	var job = make(map[string]interface{})

	job["Id"] = satellite.id
	job["Service"] = satellite.service
	job["Description"] = satellite.description
	job["Address"] = satellite.address
	job["Enabled"] = satellite.enabled

	return job
}

func (s *Satellite) Id() string {
	return s.id
}

func (s *Satellite) SetId(id string) {
	if len(id) < 1 {
		uuid, _ := NewV4()
		id = uuid.String()
	}

	s.id = id
}

func (s *Satellite) Service() string {
	return s.service
}

func (s *Satellite) SetService(service string) error {
	if len(service) < 1 {
		return model.EMPTY_STRING
	}

	s.service = service
	return nil
}

func (s *Satellite) Description() string {
	return s.description
}

func (s *Satellite) SetDescription(description string) {
	s.description = description
}

func (s *Satellite) Address() string {
	return s.address
}

func (s *Satellite) SetAddress(address string) error {
	if len(address) < 1 {
		return model.EMPTY_STRING
	}

	s.address = address
	return nil
}

func (s *Satellite) Enabled() bool {
	return s.enabled
}

func (s *Satellite) SetEnabled(enabled bool) {
	s.enabled = enabled
}
