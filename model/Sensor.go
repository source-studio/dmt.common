package model

import (
	"gxs3/godfather/model"
)

const (
	PH           SensorType = "ph"               // PH sensor type constant
	CONDUCTIVITY SensorType = "conductivy"       // Conductivity sensor type constant
	TEMPERATURE  SensorType = "temperature"      // Temperature sensor type constant
	HUMIDITY     SensorType = "humidity"         // Humidity sensor type constant
	RADIATION    SensorType = "solar-radiation"  // Solar radiation sensor type constant
	OXYGEN       SensorType = "dissolved-oxygen" // Dissolved Oxygen sensor type constant
	FLOW         SensorType = "flow-meter"       // Flow meter sensor type constant
	PRESSURE     SensorType = "pressure"         // Barometric pressure sensor type constant
)

/*
 Represents a sensor that will be connected to a satellite to collect readings.
*/
type Sensor struct {
	*model.AbstractModel
	idSatellite string     // Satellite this sensor is connected to
	stype       SensorType // Type of sensor
	on          bool       // Is this sensor on? If off, no readings should be taken from it
	trigger     *Trigger   // Trigger condition that indicates when this sensor should take readings
	connector   *Connector // Provides connection info. The adapter will use this to connect to the sensor.
}

/*
Used to enumerate the types of sensors that the satellites are prepared to interact with and take readings from. One
sensor can produce multiple types of readings.
*/
type SensorType string

/*
 Creates a new sensor
*/
func NewSensor(id uint, idSatellite string, stype SensorType, on bool, trigger *Trigger, connector *Connector) (*Sensor, error) {
	if len(idSatellite) < 1 || len(stype) < 1 {
		return nil, model.EMPTY_STRING
	}

	if trigger == nil || connector == nil {
		return nil, model.NIL_ARGUMENT
	}

	var s = &Sensor{model.NewModel(id), idSatellite, stype, on, trigger, connector}
	return s, nil
}

/*
 Decodes a sensor from a json object
*/
func DecodeSensor(job map[string]interface{}) (interface{}, error) {
	if job == nil {
		return nil, nil
	}

	var ok = make([]bool, 6)
	var id, value, baud float64
	var idSatellite, stype, ttype, port string
	var on bool
	var tjob, cjob map[string]interface{}

	id, ok[0] = job["Id"].(float64)
	idSatellite, ok[1] = job["IdSatellite"].(string)
	stype, ok[2] = job["Stype"].(string)
	on, ok[3] = job["on"].(bool)
	tjob, ok[4] = job["Trigger"].(map[string]interface{})
	cjob, ok[5] = job["Connector"].(map[string]interface{})

	for _, val := range ok {
		if !val {
			return nil, model.BAD_VALUE
		}
	}

	ttype, ok[0] = tjob["Ttype"].(string)
	value, _ = tjob["Value"].(float64)

	if !ok[0] {
		return nil, model.BAD_VALUE
	}

	var trigger, err = NewTrigger(
		TriggerType(ttype),
		float32(value),
	)

	if err != nil {
		return nil, err
	}

	port, ok[0] = cjob["Port"].(string)
	baud, ok[1] = cjob["Baud"].(float64)

	for cont := 0; cont < 2; cont++ {
		if !ok[cont] {
			return nil, model.BAD_VALUE
		}
	}

	connector, err := NewConnector(
		port,
		int(baud),
	)

	sensor, err := NewSensor(
		uint(id),
		idSatellite,
		SensorType(stype),
		on,
		trigger,
		connector,
	)

	return sensor, err
}

/*
Encodes a sensor to a json object
*/
func EncodeSensor(s interface{}) map[string]interface{} {
	if s == nil {
		return nil
	}

	var sensor = s.(*Sensor)
	var job = make(map[string]interface{})
	var tjob = make(map[string]interface{})

	job["Id"] = sensor.Id()
	job["IdSatellite"] = sensor.idSatellite
	job["Stype"] = sensor.stype
	job["On"] = sensor.on
	job["Trigger"] = tjob

	tjob["Ttype"] = sensor.trigger.ttype

	if sensor.trigger.value > 0 {
		tjob["Value"] = sensor.trigger.value
	}

	job["Connector"] = map[string]interface{}{
		"Port": sensor.connector.port,
		"Baud": sensor.connector.baud,
	}

	return job
}

func (s *Sensor) IdSatellite() string {
	return s.idSatellite
}

func (s *Sensor) SetIdSatellite(idSatellite string) error {
	if len(idSatellite) < 1 {
		return model.EMPTY_STRING
	}

	s.idSatellite = idSatellite
	return nil
}

func (s *Sensor) Type() SensorType {
	return s.stype
}

func (s *Sensor) SetType(stype SensorType) error {
	if len(stype) < 1 {
		return model.EMPTY_STRING
	}

	s.stype = stype
	return nil
}

func (s *Sensor) On() bool {
	return s.on
}

func (s *Sensor) SetOn(on bool) {
	s.on = on
}

func (s *Sensor) Trigger() *Trigger {
	return s.trigger
}

func (s *Sensor) SetTrigger(trigger *Trigger) error {
	if trigger == nil {
		return model.NIL_ARGUMENT
	}

	s.trigger = trigger
	return nil
}

func (s *Sensor) Connector() *Connector {
	return s.connector
}

func (s *Sensor) SetConnector(connector *Connector) error {
	if connector == nil {
		return model.NIL_ARGUMENT
	}

	s.connector = connector
	return nil
}
