package model

import (
	"gxs3/godfather/model"
)

const (
	TIME  TriggerType = "time"  // Timer or Ticker based trigger. Will be raised after a time lapse, or on a fixed schedule
	ALARM TriggerType = "alarm" // Alarm trigger. Will wait for input from the sensor before attempting to read.
	EVENT TriggerType = "event" // Event trigger. Will wait for internal notification in the app before attempting to read.
)

/*
 Triggers are used to determine when a satellite should interact with a sensor to get one or more readings. Different
 sensors behave in different ways, and work differently in separate contexts. Satellites are configured to have varying
 triggers to determine when a sensor should provide readings.
*/
type Trigger struct {
	ttype TriggerType // Trigger type
	value float32     // Trigger value
}

/*
 Used to enumerate the different types of triggers that will be used in the application
*/
type TriggerType string

/*
 Creates a new trigger
*/
func NewTrigger(ttype TriggerType, value float32) (*Trigger, error) {
	if len(ttype) < 1 {
		return nil, model.EMPTY_STRING
	}

	if value < 0 {
		return nil, model.NEGATIVE_ARGUMENT
	}

	var t = &Trigger{ttype, value}
	return t, nil
}

func (t *Trigger) Type() TriggerType {
	return t.ttype
}

func (t *Trigger) SetType(ttype TriggerType) error {
	if len(ttype) < 1 {
		return model.NIL_ARGUMENT
	}

	t.ttype = ttype
	return nil
}

func (t *Trigger) Value() float32 {
	return t.value
}

func (t *Trigger) SetValue(value float32) error {
	if value <= 0 {
		return model.OUT_OF_RANGE
	}

	t.value = value
	return nil
}
