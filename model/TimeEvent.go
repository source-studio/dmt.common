package model

import (
	"fmt"
	"time"
)

const (
	TIME_EVENT EventType = "time" // Time event type constant
)

/*
 Time events are used for scenarios based on timers and tickers. Events that should be raised after a lapse of time
 or on a fixed schedule over time.
*/
type TimeEvent struct {
	*BaseEvent
	value time.Time // Timestamp when the event was raised. Used as the value driving the event.
}

/*
 Creates a new time event
*/
func NewTimeEvent(value time.Time) (*TimeEvent, error) {
	var event, err = NewBaseEvent(TIME_EVENT)

	if err != nil {
		return nil, err
	}

	return &TimeEvent{event, value}, nil
}

func (te *TimeEvent) String() string {
	return te.BaseEvent.String() + fmt.Sprintf(" - Value: %f", te.value)
}

func (re *TimeEvent) Value() time.Time {
	return re.value
}

func (re *TimeEvent) SetValue(value time.Time) {
	re.value = value
}
